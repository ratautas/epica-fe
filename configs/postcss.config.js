module.exports = {
  plugins: [
    require('autoprefixer')({
      browsers: [
        ">0.25%",
        "not op_mini all"
      ]
    }),
    // require("css-mqpacker"),
    require('cssnano')({
      preset: 'default'
    })
  ]
}
