<?php
  $arr = array(
    'is_more' => true,
    // 'is_more' => false,
    'articles' => array(
      array(
        'article_title' => 'EPICA awards world cup category kicks off again',
        'article_date' => 'September 13, 2018',
        'article_thumb' => '../media/card--2.png',
        'article_link' => 'article',
        'category_name' => 'EPICA NEWS',
        'category_link' => 'news',
      ),
      array(
        'article_title' => 'EPICA awards world cup category kicks off again',
        'article_date' => 'September 13, 2018',
        'article_thumb' => '../media/card--1.png',
        'article_link' => 'article',
        'category_name' => 'EPICA NEWS',
        'category_link' => 'news',
      ),
      array(
        'article_title' => 'EPICA awards world cup category kicks off again',
        'article_date' => 'September 13, 2018',
        'article_thumb' => '../media/card--1.png',
        'article_link' => 'article',
        'category_name' => 'EPICA NEWS',
        'category_link' => 'news',
      ),
      array(
        'article_title' => 'EPICA awards world cup category kicks off again',
        'article_date' => 'September 13, 2018',
        'article_thumb' => '../media/card--3.png',
        'article_link' => 'article',
        'category_name' => 'EPICA NEWS',
        'category_link' => 'news',
      ),
    ),
  );

  echo json_encode($arr); // {"a":1,"b":2,"c":3,"d":4,"e":5}
