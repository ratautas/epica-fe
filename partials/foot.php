<?php $localhost = file_get_contents('http://0.0.0.0:5000/assets/js/app.js') ? true : false; ?>

<i class="app__mask" data-mask></i>
<footer class="app__footer footer">
  <?php include 'footer.php'; ?>
</footer>
</main>
<i class="app__overlay" data-overlay></i>
<div class="app__visual visual" id="visual" data-turbolinks-permanent
  style="background-image:url('../media/piramid_background.jpg')">
  <i class="visual__overlay" data-visual-overlay></i>
  <video class="visual__video" data-visual-video loop playsinline autoplay muted poster="../media/poster.png">
    <source src="../media/epica_pyramid_optimized.mp4" type="video/mp4">
  </video>
</div>
<?php include 'modals.php'; ?>
<?php include 'list.php'; ?>
</body>

</html>