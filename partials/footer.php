<div class="footer__container container">
  <div class="footer__quick-links">
    <a href="mailto:contact@epica-awards.com" class="footer__quick-link underlink">Site feedback</a>
    <a href="privacy" class="footer__quick-link underlink">Privacy policy</a>
    <a href="material" class="footer__quick-link underlink">Press material</a>
    <a href="about#contact" class="footer__quick-link underlink">Contact</a>
  </div>
  <div class="footer__socials">
    <a href="" target="_blank" rel="extrenal nofollow" class="footer__social"
      style="background-image:url('../assets/img/icon--facebook.svg')"></a>
    <a href="" target="_blank" rel="extrenal nofollow" class="footer__social"
      style="background-image:url('../assets/img/icon--linkedin.svg')"></a>
    <a href="" target="_blank" rel="extrenal nofollow" class="footer__social"
      style="background-image:url('../assets/img/icon--twitter.svg')"></a>
    <a href="" target="_blank" rel="extrenal nofollow" class="footer__social"
      style="background-image:url('../assets/img/icon--instagram.svg')"></a>
  </div>
  <div class="footer__partners">
    <a href="" target="_blank" rel="extrenal nofollow"
      class="footer__partner footer__partner--s"
      style="background-image:url('../media/euro_news.svg')"></a>
    <a href="" target="_blank" rel="extrenal nofollow"
      class="footer__partner footer__partner--l"
      style="background-image:url('../media/adforum.svg')"></a>
    <a href="" target="_blank" rel="extrenal nofollow"
      class="footer__partner"
      style="background-image:url('../media/adobe_stock.svg')"></a>
  </div>
  <div class="footer__copyright">1987-2019 © Epica awards. All rights reserved</div>
</div>