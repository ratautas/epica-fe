<?php $localhost = file_get_contents('http://0.0.0.0:5000/assets/js/app.js') ? true : false; ?>

<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other" class="preload">

<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
  <title>epica</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="msapplication-TileImage" content="../assets/img/favicons/ms-icon-144x144.png">
  <link rel="shortcut icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicons/favicon-16x16.png">
  <link rel="mask-icon" href="../assets/img/favicons/safari-pinned-tab.svg" color="#02a0d3">
  <meta name="msapplication-TileColor" content="#02a0d3">
  <meta name="theme-color" content="#02a0d3">
  <meta name="msapplication-tap-highlight" content="no" />
  <meta name="robots" content="noindex, nofollow">
  <?php if (!$localhost): ?>
  <link rel="stylesheet" href="../assets/css/app.css">
  <?php endif; ?>
  <?php if (!$localhost): ?>
  <script async defer type="text/javascript" src="../assets/js/app.js?<?php echo date_timestamp_get(date_create());?>"></script>
  <?php else: ?>
  <script type="text/javascript" src="http://0.0.0.0:5000/assets/js/app.js"></script>
  <script id="__bs_script__">
    //<![CDATA[
    document.write(
      "<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>"
      .replace(
        "HOST", location.hostname));
    //]]>
  </script>
  <?php endif; ?>
</head>

<body class="app" data-lang="en" data-root="http://localhost/epica-fe">