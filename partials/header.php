<header class="app__header header sticker" id="header" data-header data-turbolinks-permanent>
  <div class="header__cookiebar cookiebar" data-cookiebar="../endpoints/cookiebar.php">
    <div class="cookiebar__content">
      <div class="cookiebar__text _wysiwyg">
        <p>This website uses cookies to ensure you get the best experience.</p>
      </div>
      <div class="cookiebar__actions">
        <a class="cookiebar__action btn" href="privacy">
          <span class="btn__text">Privacy policy</span>
        </a>
        <div class="cookiebar__action btn btn--red" data-cookiebar-accept>
          <span class="btn__text">Got it</span>
        </div>
      </div>
    </div>
  </div>
  <div class="header__container">
    <a class="header__logo" href="home" style="background-image:url('../assets/img/logo.svg')"></a>
    <div class="header__navicon navicon" tabindex="0" data-navicon>
      <i class="navicon__bar navicon__bar--top"></i>
      <i class="navicon__bar navicon__bar--mid"></i>
      <i class="navicon__bar navicon__bar--bottom"></i>
    </div>
    <div class="header__nav nav">
      <div class="nav__content">
        <ul class="nav__list">
          <li class="nav__item" data-nav-item>
            <span class="nav__trigger underlink" data-nav-trigger>About</span>
            <div class="nav__submenu" data-nav-submenu>
              <ul class="nav__sublist">
                <li class="nav__subitem">
                  <a href="about#who-we-are" class="nav__subtrigger underlink">01 Who we are</a>
                </li>
                <li class="nav__subitem">
                  <a href="about#jury" class="nav__subtrigger underlink">02 Jury</a>
                </li>
                <li class="nav__subitem">
                  <a href="about#testimonials" class="nav__subtrigger underlink">03 Testimonials</a>
                </li>
                <li class="nav__subitem">
                  <a href="about#store" class="nav__subtrigger underlink">04 Store</a>
                </li>
                <li class="nav__subitem">
                  <a href="about#contact" class="nav__subtrigger underlink">05 Contact</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav__item">
            <a href="news" class="nav__trigger underlink">News</a>
          </li>
          <li class="nav__item" data-nav-item>
            <span class="nav__trigger underlink" data-nav-trigger>Enter</span>
            <div class="nav__submenu" data-nav-submenu>
              <ul class="nav__sublist">
                <li class="nav__subitem">
                  <a href="enter#call-for-entries" class="nav__subtrigger underlink">01 Call for
                    entries</a>
                </li>
                <li class="nav__subitem">
                  <a href="enter#categories" class="nav__subtrigger underlink">02 Categories</a>
                </li>
                <li class="nav__subitem">
                  <a href="enter#rules" class="nav__subtrigger underlink">03 Rules</a>
                </li>
                <li class="nav__subitem">
                  <a href="enter#pricing" class="nav__subtrigger underlink">04 Pricing</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav__item">
            <a href="results" class="nav__trigger underlink">Results</a>
          </li>
          <li class="nav__item" data-nav-item>
            <span class="nav__trigger underlink" data-nav-trigger>Publications</span>
            <div class="nav__submenu" data-nav-submenu>
              <ul class="nav__sublist">
                <li class="nav__subitem">
                  <a href="publications#book" class="nav__subtrigger underlink">01 Epica book</a>
                </li>
                <li class="nav__subitem">
                  <a href="publications#tributes" class="nav__subtrigger underlink">02 Epica
                    tributes</a>
                </li>
                <li class="nav__subitem">
                  <a href="publications#press" class="nav__subtrigger underlink">03 Press
                    coverage</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav__item">
            <a href="https://epica.adforum.com/profile/auth/login/__press__center__" target="_blank"
              rel="extrenal nofollow" class="nav__trigger underlink">Press club</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>