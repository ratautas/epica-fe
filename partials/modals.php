<div class="app__modal modal" data-modal="jury1">
  <i class="modal__backdrop" data-modal-close></i>
  <div class="modal__dialog">
    <i class="modal__close" data-modal-close></i>
    <div class="modal__content">
      <div class="modal__info">
        <div class="modal__title">Adformatie</div>
        <div class="modal__meta">Netherlands</div>
        <div class="modal__text">
          <p>Adformatie is the online platform for the professionals who deal with communication.
            Here you will find the most important and up-to-date news about advertising, media and
            marketing.</p>
        </div>
      </div>
      <div class="modal__media media">
        <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury media__image--modal">
      </div>
      <div class="modal__actions">
        <a class="modal__action btn">
          <span class="btn__text">Visit website</span>
        </a>
      </div>
    </div>
  </div>
</div>