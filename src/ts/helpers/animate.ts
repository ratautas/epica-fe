export const animate = ($el, styleAttr, finalValue, duration, callback?) => {
  const startTime = +new Date();
  let delta = null;
  let req = null;
  let elapsedTime = 0;

  (function timeout() {
    elapsedTime = +new Date() - startTime;

    if (elapsedTime >= duration) {
      cancelAnimationFrame(req);
      $el.style[styleAttr] = `${finalValue}px`;
      if (typeof callback !== 'undefined') {
        callback();
        return;
      }
    } else {
      delta = finalValue / duration;
      $el.style[styleAttr] = `${delta * elapsedTime}px`;
    }

    req = requestAnimationFrame(timeout);
  })();
};
