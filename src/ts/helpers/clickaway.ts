export const clickaway = ($el, callback) => {
  const outsideClickListener = (e) => {
    if (!$el.contains(e.target) && e.target.closest($el) === null) {
      callback();
      document.removeEventListener('click', outsideClickListener);
    }
  };
  document.addEventListener('click', outsideClickListener);
};
