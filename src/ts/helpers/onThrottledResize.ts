import { throttleRAF } from '../helpers/throttleRAF';

export const onThrottledResize = (action: Function) => {
  window.addEventListener('resize', throttleRAF(action));
};
