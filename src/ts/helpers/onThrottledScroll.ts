import { throttleRAF } from '../helpers/throttleRAF';

export const onThrottledScroll = (action: Function) => {
  window.addEventListener('scroll', throttleRAF(action));
};
