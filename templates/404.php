<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__notfound notfound">
    <div class="notfound__title appear" data-appear>404</div>
    <div class="notfound__subtitle appear" data-appear>Page not found</div>
    <div class="notfound__text _wysiwyg appear" data-appear>
      <p>The Page you are looking for doesn’t exist or an other error occurred.</p>
      <p>Go back, or head over to epica <a href="epica-awards.com">epica-awards.com</a>.</p>
    </div>
  </div>
  <?php include '../partials/foot.php';
