<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__heading appear" data-sticky data-appear>
    <h1>About</h1>
  </div>
  <div class="app__expands" data-expand>
    <section class="app__expand expand" data-appear data-expand-item="who-we-are">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">01</span>
          <h3>Who we are</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--who" data-expand-content>
        <div class="expand__who who">
          <div class="who__intro _wysiwyg">
            <p>Epica is the point where the creative industries (advertising, design, media, PR and
              digital) meet the world’s marketing and communications press.
            </p>
          </div>
          <div class="who__block">
            <div class="who__columns who__columns--full">
              <div class="who__column who__column--third _wysiwyg">
                <p>Established in 1987, Epica is unique in the crowded awards sector as it is the
                  only prize judged by journalists working for marketing and communications titles.
                  More than 200 magazines and websites from around the world are on our jury.</p>
              </div>
              <div class="who__column who__column--third _wysiwyg">
                <p>Epica’s aim is to reward outstanding creativity and help agencies, production
                  companies, media consultancies, photographers and design studios to develop their
                  reputations beyond their national borders, attracting thousands of entries from
                  more than 70 countries.</p>
              </div>
              <div class="who__column who__column--third _wysiwyg">
                <p>Epica offers links with and exposure to an unrivalled network of journalists who
                  are specialists in their field.</p>
              </div>
            </div>
          </div>
          <div class="who__media">
            <img src="../media/who--1.png" alt="Jury" class="media__image media__image--jury">
          </div>
          <div class="who__block">
            <div class="who__columns who__columns--two-thirds">
              <div class="who__subtitle">History</div>
              <div class="who__column who__column--half _wysiwyg">
                <p>A lot has changed since the birth of the Epica Awards. But from the very
                  beginning, every entry has been judged on the strength of the creative idea and
                  the quality of its execution.</p>
                <p>The Epica Awards were created in 1987 by former ad man Andrew Rawlins. And like
                  the world’s best advertising, Epica sprang out of a great idea. Or rather, two of
                  them.</p>
                <p>The first was to promote and reward the highest standards in European advertising
                  – at the time, Epica was unique in doing so. The second was to recruit the jury,
                  not from the advertising industry, but from the advertising trade press. Reporters
                  who spent their lives analysing and critiquing communications would have a chance
                  to honour creativity in their sector. Once again, this was unprecedented.</p>
              </div>
              <div class="who__column who__column--half _wysiwyg">
                <p>Today, Epica is open to agencies worldwide and judged by more than 60 journalists
                  from magazines and websites across the globe. Back at the start, the jury was 15
                  strong and the prizes were presented at the Brussels headquarters of the European
                  Commission.</p>
                <p>The very first Grand Prix went to the London agency Boase Massimi Pollitt for a
                  TV spot for the Guardian newspaper. The Paris agency Creative Business won the
                  pan-European prize for a campaign for Louis Vuitton. 776 campaigns were entered by
                  192 agencies.</p>
                <p>The winners were featured in the inaugural edition of the Epica Book, a luxurious
                  publication sent out free of charge to all who enter.</p>
              </div>
            </div>
            <div class="who__columns who__columns--third">
              <div class="who__subtitle">Benefits</div>
              <div class="who__column who__column--full _wysiwyg">
                <p>Entering Epica allows your work to be judged by an independent and unbiased jury,
                  whose members combine the objectivity of the public with the critical gaze of
                  experts.</p>
                <p>Over the years, work that has won its first award at Epica has gone on to
                  reap prizes at other creative awards throughout the season, suggesting that Epica
                  is a barometer of industry trends. All entrants to Epica receive a copy of the
                  Epica Book, a sumptuous bible of creativity and a source of inspiration, published
                  by Bloomsbury.</p>
                <p>Beyond the awards, Epica’s mission is to strengthen the relationship between
                  creative businesses and the marketing press.</p>
              </div>
            </div>
          </div>
          <div class="who__media">
            <img src="../media/who--2.png" alt="" class="media__image media__image--jury">
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="jury">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">02</span>
          <h3>Jury</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--jury" data-expand-content>
        <div class="expand__jury jury">
          <div class="jury__content">
            <div class="jury__actions">
              <a class="jury__action btn" href="jury">
                <span class="btn__text">See all jury</span>
              </a>
            </div>
            <div class="jury__text _wysiwyg">
              <p>Part of what has made the Epica Awards stand out for 30 years is its jury. The
                Epica Awards is the only global creative prize judged by journalists. The backbone
                of this unique group is made up of senior editors and leading journalists from the
                most prestigious trade press titles in over 50 countries. Over the years, Epica has
                expanded to include specialized titles voting on specific categories related to
                their industry, as well as online publications and influencers. Their common
                denominator: an objective and unbiased expertise as long-standing observers of
                creativity and innovation.</p>
            </div>
          </div>
          <div class="jury__slider swiper-container" data-jury-slider>
            <div class="jury__wrapper swiper-wrapper">
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--1.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Brain</div>
                    <div class="jury__country">Japan</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--2.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adindex</div>
                    <div class="jury__country">Russian Federation</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--3.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adformatie</div>
                    <div class="jury__country">Netherlands</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--4.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">+Design</div>
                    <div class="jury__country">Greece</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--5.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adobo magazine</div>
                    <div class="jury__country">Philippines</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--6.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Digiday USA</div>
                    <div class="jury__country">United States</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--4.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">+Design</div>
                    <div class="jury__country">Greece</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--5.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adobo magazine</div>
                    <div class="jury__country">Philippines</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--6.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Digiday USA</div>
                    <div class="jury__country">United States</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--6.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Digiday USA</div>
                    <div class="jury__country">United States</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
              <div class="jury__slide swiper-slide">
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--5.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adobo magazine</div>
                    <div class="jury__country">Philippines</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
                <div class="jury__member jury__member--slide">
                  <div class="jury__media media">
                    <img src="../media/jury__image--5.png" alt=""
                      class="media__image media__image--jury">
                  </div>
                  <div class="jury__meta">
                    <div class="jury__company">Adobo magazine</div>
                    <div class="jury__country">Philippines</div>
                  </div>
                  <a href="" class="jury__link" target="_blank" rel="extrenal nofollow"></a>
                </div>
              </div>
            </div>
            <div class="jury__state"></div>
            <i class="jury__nav jury__nav--prev">
              <span class="jury__arrow jury__arrow--prev arrow"></span>
            </i>
            <i class="jury__nav jury__nav--next">
              <span class="jury__arrow jury__arrow--next arrow"></span>
            </i>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="testimonials">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">03</span>
          <h3>Testimonials</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--testimonials" data-expand-content>
        <div class="expand__testimonials testimonials">
          <div class="testimonials__grid">
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>“Brands with the most famous advertising and communications will be more
                  famous, and brands with forgettable ideas will be forgotten. And we all know
                  where the smart brands want to be.”</p>
              </div>
              <div class="testimonials__author">- José Miguel Sokoloff, President of the Lowe
                Global Creative Council and Chief Creative Officer of Lowe/SSPS, Bogotá
              </div>
            </div>
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>“Work that points the way to the advertising of tomorrow.”</p>
              </div>
              <div class="testimonials__author">Olivier Altmann, Co-Founder, Altmann + Pacreau
              </div>
            </div>
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>“Standards for new thinking, for intelligent problem solving and for refreshing
                  insights.”</p>
              </div>
              <div class="testimonials__author">Amir Kassaei, Chief Creative Officer, DDB Group,
                Germany</div>
            </div>
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>"It is journalism that ensures that the stories behind groundbreaking creative
                  ideas are shared publicly...I look forward to being challenged by the brilliant
                  creative ideas featured on the pages of publications such as Epica for years to
                  come."</p>
              </div>
              <div class="testimonials__author">Kate Stanners, Chairwoman and Global Chief
                Creative
                Officer of Saatchi & Saatchi</div>
            </div>
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>“Work that points the way to the advertising of tomorrow.”</p>
              </div>
              <div class="testimonials__author">Olivier Altmann, Co-Founder, Altmann + Pacreau
              </div>
            </div>
            <div class="testimonials__item">
              <div class="testimonials__quote _wysiwyg" data-equalize="quote">
                <p>“Standards for new thinking, for intelligent problem solving and for refreshing
                  insights.”</p>
              </div>
              <div class="testimonials__author">Amir Kassaei, Chief Creative Officer, DDB Group,
                Germany</div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="store">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">04</span>
          <h3>Store</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--store" data-expand-content>
        <div class="expand__store store">
          <div class="store__media media">
            <img src="../media/store__image.png" alt="" class="media__image media__image--store" />
          </div>
          <div class="store__content">
            <div class="store__text _wysiwyg">
              <p>If you would like to order</p>
              <p>
                <ul>
                  <li>A duplicate of your original award</li>
                  <li>A duplicate of your certificat</li>
                  <li>An Epica Book</li>
                  <li>A Brand tribute</li>
                </ul>
              </p>
              <p>You’re in the right place! Just download the relevant.</p>
            </div>
            <a href="https://d2vwr3m1p5zx62.cloudfront.net/staticfiles/Trophy%20and%20certificate%20order%20form042786a694.pdf"
              download target="_blank" rel="extrenal nofollow" class="store__download btn">
              <span class="btn__text">Download PDF</span>
              <i class="btn__icon btn__icon--l btn__icon--right">
                <span class="btn__download download"></span>
              </i>
            </a>
            <div class="store__text _wysiwyg">
              <p>Download, print and fill in the form, then scan and email it to us at
                <a href="mailto:shop@epica-awards.com">shop@epica-awards.com</a></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="contact">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">05</span>
          <h3>Contact</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--contact" data-expand-content>
        <div class="expand__contact contact">
          <div class="contact__columns">
            <div class="contact__column">
              <div class="contact__item">
                <i class="contact__icon"
                  style="background-image:url('../assets/img/icon--house.svg')"></i>
                <div class="contact__label">Epica awards
                  112 bis, rue Cardinet, 75017
                  Paris – France</div>
              </div>
            </div>
            <div class="contact__column">
              <div class="contact__item">
                <i class="contact__icon"
                  style="background-image:url('../assets/img/icon--phone.svg')"></i>
                <a href="tel:+33 (0)1 42 04 04 32" class="contact__label">+33 (0)1 42 04 04 32</a>
              </div>
              <div class="contact__item">
                <i class="contact__icon"
                  style="background-image:url('../assets/img/icon--mail.svg')"></i>
                <a href="mailto:contact@epica-awards.com"
                  class="contact__label">contact@epica-awards.com</a>
              </div>
            </div>
          </div>
          <div class="contact__newsletter">
            <form class="newsletter__form form"
              action="http://localhost/epica-fe/endpoints/newsletter.php">
              <div class="form__content" data-form-content>
                <div class="form__intro">Sign up to our news</div>
                <div class="form__row">
                  <div class="form__item">
                    <input type="email" name="email" placeholder="Enter your email" required
                      class="form__input">
                    <span class="form__error">Error</span>
                    <button class="form__submit btn">
                      <i class="btn__arrow arrow"></i>
                    </button>
                  </div>
                </div>
              </div>
              <div class="form__done form__done--success" data-form-done="success">Success!</div>
              <div class="form__done form__done--error" data-form-done="error">Error:(</div>
            </form>
          </div>
        </div>
      </div>
  </div>
  </section>
  </div>
  <?php include '../partials/foot.php';
