<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <a class="app__back back appear" data-appear href="news">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to All news</span>
  </a>
  <div class="app__heading appear" data-appear data-sticky>
    <h1>When a logo is a no go</h1>
  </div>
  <div class="app__meta meta appear" data-appear>
    <span class="meta__author">Article by Mark Tungate</span>
    <span class="meta__date">Thursday 21 February 2019</span>
  </div>
  <div class="app__share app__share--top share appear" data-appear>
    <div class="share__label">Share on:</div>
    <div class="share__icons">
      <i class="share__icon">
        <img src="../assets/img/icon--facebook.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--linkedin.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--twitter.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
    </div>
  </div>
  <article class="app__article article">
    <section data-appear class="article__section article__section--left-media-text appear">
      <div class="article__media article__media--left media">
        <img src="../media/article__image--1.png" alt="" class="media__image">
      </div>
      <div class="article__text article__text--right _wysiwyg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
          Curabitur facilisis ex eu augue laoreet commodo. Nullam vulputate purus magna, at
          vulputate urna venenatis vitae. Vestibulum sed lacus maximus massa venenatis dapibus. Ut
          semper ante consectetur lacus dignissim elementum.</p>
        <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada
          fames ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit
          sapien placerat quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a
          lacus et urna dignissim vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi
          fringilla, neque nec euismod cursus, augue elit elementum libero, lobortis lobortis felis
          nulla sit amet mauris. Vestibulum sed nulla non elit porttitor mollis. Maecenas pretium
          fermentum tempus. Ut sed magna a diam volutpat malesuada. Duis lorem augue, porttitor nec
          ex eu, pulvinar mollis purus.</p>
        <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis
          purus convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus
          hendrerit, efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra,
          quis scelerisque dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem,
          finibus at tempus in, mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis
          purus. Ut venenatis et eros a hendrerit.</p>
        <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus
          lacus. Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus
          facilisis et. Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse
          ut ligula malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et
          luctus augue efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante,
          semper in facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra
          lobortis, nulla risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam
          congue aliquet nunc, at tincidunt massa sollicitudin nec.</p>
        <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu
          molestie arcu. Nam sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu
          condimentum. Proin cursus tempus sollicitudin. Orci varius natoque penatibus et magnis dis
          parturient montes, nascetur ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id
          malesuada. Vivamus facilisis quam quis nunc iaculis sollicitudin. Sed vulputate faucibus
          metus, quis lacinia augue imperdiet non. Aenean sagittis mollis risus, quis pharetra leo
          aliquam malesuada.
        </p>
      </div>
    </section>
    <section data-appear class="article__section article__section--full-media appear">
      <div class="article__media media">
        <img src="../media/article__image--2.png" alt="" class="media__image">
      </div>
    </section>
    <section data-appear class="article__section article__section--left-quote-text appear">
      <div class="article__quote article__quote--left">
        <blockquote>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin
          quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
        </blockquote>
      </div>
      <div class="article__text article__text--right _wysiwyg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
          Curabitur facilisis ex eu augue laoreet commodo. Nullam vulputate purus magna, at
          vulputate urna venenatis vitae. Vestibulum sed lacus maximus massa venenatis dapibus. Ut
          semper ante consectetur lacus dignissim elementum.</p>
        <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada
          fames ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit
          sapien placerat quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a
          lacus et urna dignissim vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi
          fringilla, neque nec euismod cursus, augue elit elementum libero, lobortis lobortis felis
          nulla sit amet mauris. Vestibulum sed nulla non elit porttitor mollis. Maecenas pretium
          fermentum tempus. Ut sed magna a diam volutpat malesuada. Duis lorem augue, porttitor nec
          ex eu, pulvinar mollis purus.</p>
        <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis
          purus convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus
          hendrerit, efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra,
          quis scelerisque dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem,
          finibus at tempus in, mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis
          purus. Ut venenatis et eros a hendrerit.</p>
        <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus
          lacus. Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus
          facilisis et. Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse
          ut ligula malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et
          luctus augue efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante,
          semper in facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra
          lobortis, nulla risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam
          congue aliquet nunc, at tincidunt massa sollicitudin nec.</p>
        <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu
          molestie arcu. Nam sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu
          condimentum. Proin cursus tempus sollicitudin. Orci varius natoque penatibus et magnis dis
          parturient montes, nascetur ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id
          malesuada. Vivamus facilisis quam quis nunc iaculis sollicitudin. Sed vulputate faucibus
          metus, quis lacinia augue imperdiet non. Aenean sagittis mollis risus, quis pharetra leo
          aliquam malesuada.</p>
      </div>
      </div>
    </section>
    <section data-appear class="article__section article__section--two-media appear">
      <div class="article__media article__media--half media">
        <img src="../media/article__image--3.png" alt="" class="media__image">
      </div>
      <div class="article__media article__media--half media">
        <img src="../media/article__image--4.png" alt="" class="media__image">
      </div>
    </section>
    <section data-appear class="article__section article__section--text appear">
      <div class="article__text article__text--full _wysiwyg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
          Curabitur facilisis ex eu augue laoreet commodo. Nullam vulputate purus magna, at
          vulputate urna venenatis vitae. Vestibulum sed lacus maximus massa venenatis dapibus. Ut
          semper ante consectetur lacus dignissim elementum.</p>
        <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada
          fames ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit
          sapien placerat quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a
          lacus et urna dignissim vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi
          fringilla, neque nec euismod cursus, augue elit elementum libero, lobortis lobortis felis
          nulla sit amet mauris. Vestibulum sed nulla non elit porttitor mollis. Maecenas pretium
          fermentum tempus. Ut sed magna a diam volutpat malesuada. Duis lorem augue, porttitor nec
          ex eu, pulvinar mollis purus.</p>
        <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis
          purus convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus
          hendrerit, efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra,
          quis scelerisque dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem,
          finibus at tempus in, mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis
          purus. Ut venenatis et eros a hendrerit.</p>
        <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus
          lacus. Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus
          facilisis et. Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse
          ut ligula malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et
          luctus augue efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante,
          semper in facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra
          lobortis, nulla risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam
          congue aliquet nunc, at tincidunt massa sollicitudin nec.</p>
        <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu
          molestie arcu. Nam sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu
          condimentum. Proin cursus tempus sollicitudin. Orci varius natoque penatibus et magnis dis
          parturient montes, nascetur ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id
          malesuada. Vivamus facilisis quam quis nunc iaculis sollicitudin. Sed vulputate faucibus
          metus, quis lacinia augue imperdiet non. Aenean sagittis mollis risus, quis pharetra leo
          aliquam malesuada.</p>
      </div>
    </section>
    <section data-appear class="article__section article__section--full-media appear">
      <div class="article__media media">
        <img src="../media/article__image--5.png" alt="" class="media__image">
      </div>
    </section>
    <section data-appear class="article__section article__section--single-quote appear">
      <div class="article__quote">
        <blockquote>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
        </blockquote>
      </div>
    </section>
    <section data-appear class="article__section article__section--left-text-media appear">
      <div class="article__text article__text--left _wysiwyg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
          Curabitur facilisis ex eu augue laoreet commodo. Nullam vulputate purus magna, at
          vulputate urna venenatis vitae. Vestibulum sed lacus maximus massa venenatis dapibus. Ut
          semper ante consectetur lacus dignissim elementum.</p>
        <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada
          fames ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit
          sapien placerat quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a
          lacus et urna dignissim vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi
          fringilla, neque nec euismod cursus, augue elit elementum libero, lobortis lobortis felis
          nulla sit amet mauris. Vestibulum sed nulla non elit porttitor mollis. Maecenas pretium
          fermentum tempus. Ut sed magna a diam volutpat malesuada. Duis lorem augue, porttitor nec
          ex eu, pulvinar mollis purus.</p>
        <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis
          purus convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus
          hendrerit, efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra,
          quis scelerisque dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem,
          finibus at tempus in, mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis
          purus. Ut venenatis et eros a hendrerit.</p>
        <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus
          lacus. Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus
          facilisis et. Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse
          ut ligula malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et
          luctus augue efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante,
          semper in facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra
          lobortis, nulla risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam
          congue aliquet nunc, at tincidunt massa sollicitudin nec.</p>
        <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu
          molestie arcu. Nam sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu
          condimentum. Proin cursus tempus sollicitudin. Orci varius natoque penatibus et magnis dis
          parturient montes, nascetur ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id
          malesuada. Vivamus facilisis quam quis nunc iaculis sollicitudin. Sed vulputate faucibus
          metus, quis lacinia augue imperdiet non. Aenean sagittis mollis risus, quis pharetra leo
          aliquam malesuada.
        </p>
      </div>
      <div class="article__media article__media--right media">
        <img src="../media/article__image--1.png" alt="" class="media__image">
      </div>
    </section>
    <section data-appear class="article__section article__section--left-text-quote appear">
      <div class="article__text article__text--left _wysiwyg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
          Curabitur facilisis ex eu augue laoreet commodo. Nullam vulputate purus magna, at
          vulputate urna venenatis vitae. Vestibulum sed lacus maximus massa venenatis dapibus. Ut
          semper ante consectetur lacus dignissim elementum.</p>
        <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada
          fames ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit
          sapien placerat quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a
          lacus et urna dignissim vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi
          fringilla, neque nec euismod cursus, augue elit elementum libero, lobortis lobortis felis
          nulla sit amet mauris. Vestibulum sed nulla non elit porttitor mollis. Maecenas pretium
          fermentum tempus. Ut sed magna a diam volutpat malesuada. Duis lorem augue, porttitor nec
          ex eu, pulvinar mollis purus.</p>
        <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis
          purus convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus
          hendrerit, efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra,
          quis scelerisque dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem,
          finibus at tempus in, mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis
          purus. Ut venenatis et eros a hendrerit.</p>
        <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus
          lacus. Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus
          facilisis et. Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse
          ut ligula malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et
          luctus augue efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante,
          semper in facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra
          lobortis, nulla risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam
          congue aliquet nunc, at tincidunt massa sollicitudin nec.</p>
        <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu
          molestie arcu. Nam sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu
          condimentum. Proin cursus tempus sollicitudin. Orci varius natoque penatibus et magnis dis
          parturient montes, nascetur ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id
          malesuada. Vivamus facilisis quam quis nunc iaculis sollicitudin. Sed vulputate faucibus
          metus, quis lacinia augue imperdiet non. Aenean sagittis mollis risus, quis pharetra leo
          aliquam malesuada.
        </p>
      </div>
      <div class="article__quote article__quote--right">
        <blockquote>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus,
          sollicitudin
          quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus.
        </blockquote>
      </div>
    </section>
  </article>
  <a class="app__back back appear" data-appear href="news">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to All news</span>
  </a>
  <div class="app__share share appear" data-appear>
    <div class="share__label">Share on:</div>
    <div class="share__icons">
      <i class="share__icon">
        <img src="../assets/img/icon--facebook.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--linkedin.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--twitter.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
    </div>
  </div>
  <div class="app__subtitle appear" data-appear>
    <h2>Related articles</h2>
  </div>
  <div class="app__cards">
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--2.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--3.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--4.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--5.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <a class="app__more btn" href="news">
      <span class="btn__text">See more articles</span>
    </a>
  </div>
  <?php include '../partials/foot.php';
