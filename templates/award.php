<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <a class="app__back back appear" data-appear href="awards">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to All awards</span>
  </a>
  <div class="app__heading app__heading--award appear" data-appear data-sticky>
    <h1>BBDO Group Germany GmbH - "Remember me"</h1>
  </div>
  <div class="app__meta app__meta--award meta">
    <div class="meta__category">Germany</div>
    <div class="meta__category">Digital media</div>
    <div class="meta__award" style="background-image:url('../assets/img/award--grand.png')">Grand</div>
    <div class="meta__award" style="background-image:url('../assets/img/award--gold.png')">Gold</div>
    <div class="meta__award" style="background-image:url('../assets/img/award--silver.png')">Silver</div>
  </div>
  <div class="app__share app__share--award-top share">
    <div class="share__label">Share on:</div>
    <div class="share__icons">
      <i class="share__icon">
        <img src="../assets/img/icon--facebook.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--linkedin.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--twitter.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
    </div>
  </div>
  <div class="app__award award">
    <div class="award__featured media">
      <img src="../media/award__featured.png" alt="" class="media__image media__image--award">
    </div>
    <div class="award__subtitle">Credits</div>
    <div class="award__credits">
      <div class="award__column award__column--1">
        <div class="award__credit">
          <div class="award__label">Agency (& City)</div>
          <div class="award__value">HEIMAT, Berlin</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Creative Director(s)</div>
          <div class="award__value"> Nicolas Blättry</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Copywriter(s)</div>
          <div class="award__value">Larissa Huhnekohl</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Art Director(s)</div>
          <div class="award__value">Lukas Kölling, Ramona Hartmann</div>
        </div>
      </div>
      <div class="award__column award__column--2">
        <div class="award__credit">
          <div class="award__label">Production Company (& City)</div>
          <div class="award__value">Heimat Active Berlin, Frameless Studio</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Agency Producer(s)</div>
          <div class="award__value">Flo Hoffmann, Jacquelin Van Dongen</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Illustrator(s)</div>
          <div class="award__value">Esra Gülmen</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Chief Creative Officer</div>
          <div class="award__value">Guido Heffels, Matthias Storath</div>
        </div>
      </div>
      <div class="award__column award__column--3">
        <div class="award__credit">
          <div class="award__label">Managing Director</div>
          <div class="award__value">Ricardo Distefano</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Title</div>
          <div class="award__value">Grandma</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Product</div>
          <div class="award__value">Automotive</div>
        </div>
        <div class="award__credit">
          <div class="award__label">Advertiser</div>
          <div class="award__value">Opel</div>
        </div>
      </div>
    </div>
    <article class="award__article">
      <section class="award__section award__section--with-media">
        <div class="award__media media">
          <iframe width="100%" height="100%"
            src="https://www.adforum.com/public/afup_render/ifr/645/363/ad/T1N6ZWc2V2JVK1VpUmpzRy9KQUJaRTZ6MDVIWlFtUnZqM0l3QzM4Y0JSMXBrRHRScFNjM25oaGtpaGRUVzdlaUpYMm9hWjF6ZXhwVVdNallCcTNQbEltYmFBT015dHJMMlV5SEdwZFFFU2hiQmd4TGxoSU1sektKbWN0THJYTVFXYW1yQjdLeVkzeTRhdVRYUlZ4TjVhQkNIcnE3aTFtZUdRY1pCTFhtcnlqK0ltUENBZ0trMVppV0JOKzE1ZW5wMkl0blhoK256L1ZXWTJnRXozZGtmWENOaGRKbTdlUVQvVU9IYnFNeVFCMjZYRGlrZFZRQlhQekJnajVoRkRWRkcrK2pMRi9vSDdhRGpjdGtKRkkySTE3OHF1UytMeGw1VFI5Tlc0eThkNUdmaVc3ejIyNUhxZG5rcXhCR0MrQUUzUnNOZzNLY2o5QTBISExwU0NLN0VRPT0"
            frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="award__text _wysiwyg">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia sapien in
            justo gravida, ac mollis libero gravida. Duis luctus nisl eu lobortis egestas. Etiam
            gravida at libero at volutpat. Curabitur ut tincidunt diam. Quisque ullamcorper quam et
            ligula fermentum finibus. In hac habitasse platea dictumst. Suspendisse ante lectus,
            sollicitudin a lacinia nec, consequat ac risus. Vestibulum id rhoncus nisl, a placerat
            arcu. Sed at lacus tortor. Integer dictum ante est, in rhoncus nibh mollis in.
            Pellentesque euismod odio at nisi sagittis, non pulvinar elit placerat. Nam tortor odio,
            venenatis a finibus id, varius sed velit. Nulla euismod tempus mi, ac tristique nunc
            tristique nec. Sed sed neque magna.</p>
        </div>
      </section>
      <section class="award__section award__section--with-media">
        <div class="award__media media">
          <img src="../media/award__image--2.png" alt="" class="media__image media__image--award">
        </div>
        <div class="award__text _wysiwyg">
          <p>Pellentesque venenatis felis nec felis dictum cursus. Duis nec pulvinar sapien, sed
            laoreet justo. Pellentesque vitae risus nec nulla luctus placerat vel vel turpis. Donec
            cursus bibendum tortor, vitae faucibus sapien interdum a. Suspendisse potenti. Duis
            varius rutrum commodo. Cras est dolor, rhoncus quis imperdiet ut, maximus at est.</p>
          <p>Pellentesque tincidunt, justo in imperdiet venenatis, neque mi efficitur mauris, non
            pulvinar tortor leo ac ante. Suspendisse potenti. Nam mollis ultricies leo, ut lobortis
            mi pretium id. Pellentesque aliquam imperdiet lectus, eu dictum urna accumsan sed. Nulla
            facilisi. In bibendum ornare metus et dignissim. Praesent ac aliquam augue.</p>
        </div>
      </section>
    </article>
  </div>
  <div class="app__bottom">
    <a class="app__back back appear" data-appear href="awards">
      <i class="back__arrow arrow"></i>
      <span class="back__label">Back to All awards</span>
    </a>
    <div class="app__share app__share--bottom share">
      <div class="share__label">Share on:</div>
      <div class="share__icons">
        <i class="share__icon">
          <img src="../assets/img/icon--facebook.svg" alt="" class="share__image">
          <a href="" class="share__link"></a>
        </i>
        <i class="share__icon">
          <img src="../assets/img/icon--linkedin.svg" alt="" class="share__image">
          <a href="" class="share__link"></a>
        </i>
        <i class="share__icon">
          <img src="../assets/img/icon--twitter.svg" alt="" class="share__image">
          <a href="" class="share__link"></a>
        </i>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php';
