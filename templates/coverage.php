<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__countries app__countries--mobile btn btn--dropdown" tabindex="0">
    <span class="btn__text">All countries</span>
    <i class="btn__icon btn__icon--right btn__icon--s">
      <?php include '../assets/img/icon--caret.svg'; ?></i>
    <select class="btn__select" data-select-link>
      <option value="coverage?country=country1">Forsman & Bodenfors</option>
      <option value="coverage?country=country2">Forsman & Bodenfors2</option>
      <option value="coverage?country=country3">Forsman & Bodenfors3</option>
      <option value="coverage?country=country4">Forsman & Bodenfors4</option>
    </select>
  </div>
  <a class="app__back back appear" data-appear href="publications">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to publications</span>
  </a>
  <div class="app__heading appear" data-appear data-sticky>
    <h1>Press coverage</h1>
    <div class="app__countries app__countries--inline btn btn--dropdown" tabindex="0">
      <span class="btn__text">All countries</span>
      <i class="btn__icon btn__icon--right btn__icon--s">
        <?php include '../assets/img/icon--caret.svg'; ?></i>
      <div class="btn__dropdown dropdown">
        <div class="dropdown__list" data-scroller>
          <a href="coverage?country=country1" class="dropdown__item">All countries</a>
          <a href="coverage?country=country2" class="dropdown__item">Forsman & Bodenfors2</a>
          <a href="coverage?country=country3" class="dropdown__item">Forsman & Bodenfors3</a>
          <a href="coverage?country=country4" class="dropdown__item">Forsman & Bodenfors4</a>
        </div>
      </div>
    </div>
  </div>
  <div class="app__coverage coverage">
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Argentina</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Amsterdam será nuevamente la sede de Epica en 2019</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Thailand</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">โฆษณา Shiseido คว้า “Film Prix” Epica Awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Maxico</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards: un ángulo diferente</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Vietnam</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Vì sao quảng cáo có cảnh hôn đồng giới của Shiseido nhận
        được giải Epica danh giá?</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Japan</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Shiseido Japan’s same-sex kiss ad wins gold in Epica
        advertising awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">United States</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Shiseido Ad Featuring Sa‌me-S‌e‌‌x K‌is‌s Wins ‘Film Grand
        Prix’ Epica Awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Netherlands</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards volgend jaar weer in Amsterdam</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Netherlands</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards en Creative Circle blijven in Amsterdam</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Argentina</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Amsterdam será nuevamente la sede de Epica en 2019</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Thailand</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">โฆษณา Shiseido คว้า “Film Prix” Epica Awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Maxico</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards: un ángulo diferente</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Vietnam</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Vì sao quảng cáo có cảnh hôn đồng giới của Shiseido nhận
        được giải Epica danh giá?</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Japan</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Shiseido Japan’s same-sex kiss ad wins gold in Epica
        advertising awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">United States</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Shiseido Ad Featuring Sa‌me-S‌e‌‌x K‌is‌s Wins ‘Film Grand
        Prix’ Epica Awards</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Netherlands</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards volgend jaar weer in Amsterdam</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
    <div class="coverage__row">
      <div class="coverage__meta">
        <div class="coverage__country">Netherlands</div>
        <div class="coverage__name">Insider Latam</div>
      </div>
      <div class="coverage__heading">Epica Awards en Creative Circle blijven in Amsterdam</div>
      <div class="coverage__date">07-12-2018</div>
      <a href="" class="coverage__link" target="_blank" rel="extrenal nofollow"></a>
    </div>
  </div>
  <?php include '../partials/foot.php';
