<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container app__container--default" data-page>
  <div class="app__heading appear" data-appear data-sticky>
    <h1>When a logo is a no go</h1>
  </div>
  <div class="app__default">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ligula dapibus, sollicitudin
      quam et, rhoncus enim. Ut eget pharetra dolor. Duis vel semper tellus. Curabitur facilisis ex
      eu augue laoreet commodo. Nullam vulputate purus magna, at vulputate urna venenatis vitae.
      Vestibulum sed lacus maximus massa venenatis dapibus. Ut semper ante consectetur lacus
      dignissim elementum.</p>
    <p>Maecenas elementum lorem id risus suscipit sagittis eu non metus. Interdum et malesuada fames
      ac ante ipsum primis in faucibus. Aenean eleifend tincidunt leo, nec hendrerit sapien placerat
      quis. Nam sed orci a est tempus sollicitudin a a enim. Pellentesque a lacus et urna dignissim
      vehicula. Aliquam vel aliquet dui, eu sodales arcu. Morbi fringilla, neque nec euismod cursus,
      augue elit elementum libero, lobortis lobortis felis nulla sit amet mauris. Vestibulum sed
      nulla non elit porttitor mollis. Maecenas pretium fermentum tempus. Ut sed magna a diam
      volutpat malesuada. Duis lorem augue, porttitor nec ex eu, pulvinar mollis purus.</p>
    <p>Aliquam ac ornare ex, sit amet cursus elit. Vivamus maximus tempus dui, quis lobortis purus
      convallis at. Vestibulum vehicula finibus enim sed dignissim. Phasellus et lacus hendrerit,
      efficitur massa sed, bibendum ligula. Cras pulvinar nisi eget risus pharetra, quis scelerisque
      dolor imperdiet. Nam ac cursus est, et commodo augue. Vivamus justo sem, finibus at tempus in,
      mattis id justo. Praesent non arcu dictum, lacinia ex ut, mollis purus. Ut venenatis et eros a
      hendrerit.</p>
    <p>Duis vestibulum urna rutrum erat bibendum, id mattis magna aliquet. Maecenas et luctus lacus.
      Fusce ultricies pellentesque diam. Maecenas rhoncus ex ex, id maximus purus facilisis et.
      Aliquam erat volutpat. Nunc interdum feugiat neque vel vehicula. Suspendisse ut ligula
      malesuada, mattis purus nec, faucibus elit. Morbi vestibulum tellus magna, et luctus augue
      efficitur eu. Suspendisse hendrerit ullamcorper dignissim. Etiam urna ante, semper in
      facilisis sit amet, elementum eu libero. Vivamus varius, magna eu pharetra lobortis, nulla
      risus tristique dui, sit amet blandit neque libero vitae orci. Aliquam congue aliquet nunc, at
      tincidunt massa sollicitudin nec.</p>
    <p>Nam eu maximus felis. Aliquam eget mi posuere, sollicitudin mauris nec, convallis tellus.
      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
      Quisque scelerisque semper nisl, sed aliquet ipsum dignissim nec. Morbi eu molestie arcu. Nam
      sed mollis arcu, placerat imperdiet erat. Proin iaculis at ligula eu condimentum. Proin cursus
      tempus sollicitudin. Orci varius natoque penatibus et magnis dis parturient montes, nascetur
      ridiculus mus. Nulla facilisi. Morbi ullamcorper eget nisl id malesuada. Vivamus facilisis
      quam quis nunc iaculis sollicitudin. Sed vulputate faucibus metus, quis lacinia augue
      imperdiet non. Aenean sagittis mollis risus, quis pharetra leo aliquam malesuada.</p>
  </div>
  <?php include '../partials/foot.php';
