<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__heading appear" data-appear data-appear>
    <h1 data-appear>Enter</h1>
    <div class="app__open open">
      <span class="open__label">We are open for entries</span>
      <a href="http://registration.epica-awards.com/" target="_blank" rel="extrenal nofollow"
        class="open__action btn btn--red">
        <span class="btn__text">Enter now</span>
      </a>
    </div>
  </div>
  <div class="app__expands" data-expand>
    <section class="app__expand expand" data-appear data-expand-item="call-for-entries">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">01</span>
          <h3>Call for entries</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--call" data-expand-content>
        <div class="expand__call call">
          <div class="call__intro">Key dates 2019</div>
          <div class="call__dates">
            <div class="call__date">
              <i class="call__icon" style="background-image:url('../media/earlybird.svg')"></i>
              <div class="call__info">
                <div class="call__label">Early bird entry period (100e off):</div>
                <div class="call__value">July 1st to August 31st</div>
              </div>
            </div>
            <div class="call__date">
              <i class="call__icon" style="background-image:url('../media/entry.svg')"></i>
              <div class="call__info">
                <div class="call__label">Normal entry period:</div>
                <div class="call__value">September 1st to October 7th</div>
              </div>
            </div>
            <div class="call__date">
              <i class="call__icon" style="background-image:url('../media/shortlist.svg')"></i>
              <div class="call__info">
                <div class="call__label">Shortlist publication:</div>
                <div class="call__value">November 11th</div>
              </div>
            </div>
            <div class="call__date">
              <i class="call__icon" style="background-image:url('../media/ceremony.svg')"></i>
              <div class="call__info">
                <div class="call__label">Ceremony:</div>
                <div class="call__value">November 21st</div>
              </div>
            </div>
            <div class="call__date">
              <i class="call__icon" style="background-image:url('../media/final_results.svg')"></i>
              <div class="call__info">
                <div class="call__label">Final results:</div>
                <div class="call__value">November 22nd</div>
              </div>
            </div>
          </div>
          <div class="call__actions">
            <a href="enter#categories" class="call__action btn">
              <span class="btn__text">See all categories</span>
            </a>
            <a href="https://epica-site.s3.amazonaws.com/media/docs/Epica-Awards_EntryKit_2019.pdf"
              download target="_blank" rel="extrenal nofollow" class="call__action btn">
              <span class="btn__text">Download the 2019 entry kit</span>
            </a>
            <a href="http://registration.epica-awards.com/" target="_blank" rel="extrenal nofollow"
              class="call__action btn btn--red">
              <span class="btn__text">Enter now</span>
            </a>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="categories">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">02</span>
          <h3>Categories</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--categories" data-expand-content>
        <div class="expand__categories categories" data-categories>
          <div class="categories__top">
            <div class="categories__filter btn btn--dropdown" data-categories-filter>
              <span class="btn__text" data-categories-filter-label>Food & Drinks</span>
              <i class="btn__icon btn__icon--right btn__icon--s">
                <?php include '../assets/img/icon--caret.svg'; ?></i>
              <div class="btn__dropdown btn__dropdown--fit dropdown">
                <div class="dropdown__list" data-scroller>
                  <span data-categories-filter-trigger="category-slug-1" class="dropdown__item is-active">Food & Drinks</span>
                  <span data-categories-filter-trigger="category-slug-2" class="dropdown__item">Health, beauty & fashion</span>
                  <span data-categories-filter-trigger="category-slug-3" class="dropdown__item">Luxury & premium brands</span>
                </div>
              </div>
              <select class="btn__select" data-categories-select>
                <option value="category-slug-1" data-categories-option selected>Food & Drinks</option>
                <option value="category-slug-2" data-categories-option>Health, beauty & fashion</option>
                <option value="category-slug-3" data-categories-option>Luxury & premium brands</option>
              </select>
            </div>
          </div>
          <div class="categories__items">
            <div class="categories__item is-active" data-categories-item="category-slug-1">
              <div class="categories__title">Food & drink</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-2">
              <div class="categories__title">Health, beauty & fashion</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-3">
              <div class="categories__title">Luxury & premium brands</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-4">
              <div class="categories__title">Household products</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-5">
              <div class="categories__title">Automotive</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-6">
              <div class="categories__title">Consumer services</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-7">
              <div class="categories__title">Media & entertainment</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-8">
              <div class="categories__title">Public interest</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic Drinks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery &
                    Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts, biscuits,
                      salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-9">
              <div class="categories__title">Radio</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery
                    & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic
                    Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Confectionery
                    & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-10">
              <div class="categories__title">Direct merketing</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic
                    Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta, eggs,
                      meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet foods
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-11">
              <div class="categories__title">Media usage</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Alcoholic
                    Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-12">
              <div class="categories__title">Branded content & entertainment
              </div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Production & Brand
                    Integration</div>
                  <div class="block__content _wysiwyg">
                    <p>Operations which promote branded products or services via
                      appearances in
                      pre-existing films, television shows or other media, and
                      which enable
                      brands
                      to gain or reinforce status from the context in which they
                      are placed.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-13">
              <div class="categories__title">Craft & imagery</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products, pasta,
                      eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt, pet
                      foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts, nuts,
                      biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-14">
              <div class="categories__title">Design</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt,
                      pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits etc.
                    </p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">Food
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt,
                      pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-15">
              <div class="categories__title">Integrated campaigns</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt,
                      pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural yoghurt,
                      pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="categories__item" data-categories-item="category-slug-16">
              <div class="categories__title">Special categories</div>
              <div class="categories__blocks">
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural
                      yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Alcoholic Drinks</div>
                  <div class="block__content _wysiwyg">
                    <p>All alcoholic drinks: wine, beer, cider, spirits
                      etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Food</div>
                  <div class="block__content _wysiwyg">
                    <p>All savoury foods, including fast-food products,
                      pasta, eggs, meats,
                      oils,
                      sauces, butter, cheese, margarine, natural
                      yoghurt, pet foods etc.</p>
                  </div>
                </div>
                <div class="categories__block block">
                  <div class="block__title" data-equalize="block-title">
                    Confectionery & Snacks
                  </div>
                  <div class="block__content _wysiwyg">
                    <p>Chocolates, sugar confectionery, jams, desserts,
                      nuts, biscuits, salted
                      snacks, chewing gum, ice cream etc.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="rules">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">03</span>
          <h3>Rules</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--rules" data-expand-content>
        <div class="expand__rules rules">
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">1</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">2</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">3</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">4</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">5</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">6</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">7</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">8</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">9</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">10</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">11</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">12</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">13</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">14</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">15</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">16</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
          <div class="rules__item" data-equalize="rule">
            <i class="rules__no">17</i>
            <div class="rules__text _wysiwyg">
              <p>A film under 90 seconds that has aired on television and/or movie theaters.
                Several TV/Cinema entries can be gathered under the same campaign to be judged and
                win together. TV/Cinema entries are all eligible for the Film Grand Prix. Upload
                requirements: 1 video file (.mov or .mp4) of the actual execution.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="pricing">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">04</span>
          <h3>Pricing</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--pricing" data-expand-content>
        <div class="expand__pricing pricing">
          <div class="pricing__blocks">
            <div class="pricing__block block">
              <div class="block__title" data-equalize="pricing">Prices</div>
              <div class="block__content _wysiwyg">
                <p>Each company taking part in the awards will pay a basic registration fee of €200.
                </p>
                <p>The fee for each entry applies to each execution whether it is judged
                  individually or as part of a campaign. Print entries are charged €290, Film and
                  Radio entries are charged €335. Alternative and Digital entries are charged €369.
                  Integrated Campaigns (category 63) are charged €599. For Photographers, the entry
                  fee for Advertising Photography (category 49) is €100.</p>
                <p>Professional photographers may enter at a discount in the Advertising Photography
                  category. Their fee for each entry in this category is €100 and the company fee is
                  waived. Photographers must contact Epica directly before taking advantage of these
                  conditions.</p>
                <p>Companies based in France will be charged French TVA (VAT) at the prevailing
                  rate. Entries from all other countries are exempt from French TVA in application
                  of articles 259 A-C of the CGI (French tax code).</p>
              </div>
            </div>
            <div class="pricing__block block">
              <div class="block__title" data-equalize="pricing">Payment</div>
              <div class="block__content _wysiwyg">
                <p>Entry fees must be paid by cheque, credit card or bank transfer (see bank details
                  below). Cheques should be drawn on a bank in France. Credit card payments must be
                  made online and include a 3% charge per transaction. Transfers should include all
                  bank charges. Invoices are due for payment upon receipt, those that are not paid
                  within 30 days will be subject to a late payment fee of 2% per month.</p>
                <p>By uploading and submitting entries to the competition online, entrants undertake
                  to pay the corresponding registration and entry fees. All entries submitted to the
                  competition will be judged, and the payment of fees will be due, unless the
                  entries are withdrawn, in writing, before October 15th, 2019.</p>
              </div>
            </div>
            <div class="pricing__block block">
              <div class="block__title" data-equalize="pricing">Bank details</div>
              <div class="block__content _wysiwyg">
                <p>Beneficiary: Maydream / Epica, 112 bis rue Cardinet - 75017 Paris, France
                  Bank Name & Address: HSBC, 35 rue de Sablonville, 92200 Neuilly sur Seine, France
                  IBAN: FR76 3005 6000 1500 1520 4408 126
                  BIC/Swift Address: CCFRFRPP</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php include '../partials/foot.php';
