<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <section class="app__hero" data-hero>
    <a href="enter" class="app__star star appear" data-appear>
      <i class="star__media star__media--banner"></i>
      <i class="star__media star__media--slogan"></i>
    </a>
    <span class="app__scrolldown appear" data-appear data-scrollto=".app__badges">
      <i class="arrow"></i>
    </span>
    <div class="app__slogan appear" data-appear>The creative prize judged by journalists</div>
  </section>
  <section class="app__badges">
    <img class="app__badge appear" alt="" src="../media/icon-01.svg" data-appear />
    <img class="app__badge appear" alt="" src="../media/icon-02.svg" data-appear />
    <img class="app__badge appear" alt="" src="../media/icon-03.svg" data-appear />
    <img class="app__badge appear" alt="" src="../media/icon-04.svg" data-appear />
  </section>
  <section class="app__cards">
    <div class="app__card card card--featured appear" data-appear>
      <div class="card__media card__media--featured media">
        <i class="media__background"
          style="background-image:url('../media/article__image--1.png')"></i>
      </div>
      <div class="card__content card__content--featured">
        <div class="card__title card__title--featured">Fact or fiction: NIKE is a ‘good’ brand</div>
        <div class="card__excerpt">The international Epica Awards are back in the
          Netherlands. The goal of the interactive conference, with participants and partners
          such as Philips, PwC and Euronews, is to answer the question: Will responsibility
          (eventually) kill creativity?</div>
        <div class="card__meta card__meta--featured">
          <div class="card__date card__date--featured">September 13, 2018</div>
          <a href="news" class="card__tag card__tag--featured">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--2.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--3.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--4.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">GLOBAL HEADLINE MAKERS: JOHANNA MARCIANO (FRANCE)</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--5.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--6.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--7.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--8.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--9.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--10.png')"></i>
      </div>
      <div class="card__content" data-equalize="card-content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <a href="news" class="app__more btn">
      <div class="btn__text">More</div>
      <i class="btn__icon btn__icon--right btn__icon--s">
        <?php include '../assets/img/icon--caret.svg'; ?></i>
    </a>
  </section>
  <?php include '../partials/foot.php';
