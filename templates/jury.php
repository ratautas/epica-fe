<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <a class="app__back back appear" data-appear href="about#jury">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to Tributes</span>
  </a>
  <div class="app__heading appear" data-appear data-sticky>
    <h1>Jury list</h1>
  </div>
  <div class="app__jury jury">
    <div class="jury__grid">
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--1.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Brain</div>
          <div class="jury__country">Japan</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--2.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adindex</div>
          <div class="jury__country">Russian Federation</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--3.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adformatie</div>
          <div class="jury__country">Netherlands</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--4.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">+Design</div>
          <div class="jury__country">Greece</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--5.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Adobo magazine</div>
          <div class="jury__country">Philippines</div>
        </div>
      </div>
      <div class="jury__member" data-modal-trigger="jury1">
        <div class="jury__media media">
          <img src="../media/jury__image--6.png" alt="" class="media__image media__image--jury">
        </div>
        <div class="jury__meta">
          <div class="jury__company">Digiday USA</div>
          <div class="jury__country">United States</div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php';
