<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__heading appear" data-appear>
    <h1>Press material</h1>
  </div>
  <div class="app__intro intro" data-appear>
    <div class="intro__blocks">
      <div class="intro__block">
        <div class="intro__subtitle" data-equalize="subtitle">Help promoting EPICA</div>
        <div class="intro__text">
          <p>Thank you for helping us to promote the Epica Awards. Our “Open for Entries” campaign
            gives agencies in your country a chance to shine. Don’t hesitate to encourage them!</p>
          <p>Please link all banners to:<a
              href="http://www.epica-awards.com">http://www.epica-awards.com</a></p>
        </div>
      </div>
      <div class="intro__block">
        <div class="intro__subtitle" data-equalize="subtitle">Find the banners you need</div>
        <div class="intro__text">
          <p>This page gives you access to all the banners we’ve produced for 2017 in different
            sizes. If you need a size other than what is available here, feel free to let us know
            and we will make one for you.</p>
          <p>IF YOU DO NOT SEE THE BANNERS BELOW, PLEASE DISABLE YOUR AD BLOCKER FOR THIS SITE.</p>
        </div>
      </div>
      <div class="intro__block">
        <div class="intro__subtitle" data-equalize="subtitle"></div>
        <div class="intro__text">
          <p>Don't hesitate to contact us if you have any questions.</p>
          <p>Email: <a href="mailto:lucia@epica-awards.com">lucia@epica-awards.com</a></p>
          <p>Phone: <a href="tel:+33 (0)1 42 04 96 33">+33 (0)1 42 04 96 33</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="app__mob-filter appear" data-appear>
    <div class="btn btn--dropdown" tabindex="0">
      <span class="btn__text">All</span>
      <i class="btn__icon btn__icon--right btn__icon--s"><?php include '../assets/img/icon--caret.svg'; ?></i>
      <!--
      <div class="btn__dropdown dropdown">
        <div class="dropdown__list" data-scroller>
          <a data-filter-trigger href="material?type=print" class="dropdown__item">Print</a>
          <a data-filter-trigger href="material?type=digital" class="dropdown__item">Digital</a>
          <a data-filter-trigger href="material?type=brand" class="dropdown__item">Brand</a>
          <a data-filter-trigger href="material?type=material" class="dropdown__item">Material</a>
          <a data-filter-trigger href="material?type=pictures" class="dropdown__item">Pictures</a>
        </div>
      </div>
-->
      <select class="btn__select" data-select-link>
        <option>All</option>
        <option value="material?type=print">Print</option>
        <option value="material?type=digital">Digital</option>
        <option value="material?type=brand">Brand</option>
        <option value="material?type=material">Material</option>
        <option value="material?type=pictures">Pictures</option>
      </select>
    </div>
  </div>
  <div class="app__filters app__filters--material" data-appear>
    <a href="material" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">All</span>
    </a>
    <a href="material?category=print" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">Print</span>
    </a>
    <a href="material?category=digital" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">Digital</span>
    </a>
    <a href="material?category=brand" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">Brand</span>
    </a>
    <a href="material?category=material" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">Material</span>
    </a>
    <a href="material?category=pictures" data-filter-trigger class="app__filter btn" tabindex="0">
      <span class="btn__text">Pictures</span>
    </a>
    <div class="app__range range" data-appear>
      <div class="range__label">Width Range</div>
      <div class="range__slider" data-range="px" data-range-min="0" data-range-max="2000"></div>
    </div>
  </div>
  <div class="app__material material" data-appear>
    <div class="material__grid">
      <div class="material__item" data-range-size="100">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--1.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="200">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--2.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="300">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--3.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="400">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--4.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="900">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--1.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1000">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--2.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1100">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--3.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>

        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1200">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--4.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1300">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--1.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1400">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--2.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1500">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--3.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1600">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--4.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1700">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--1.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1800">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--2.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="1900">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--3.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
      <div class="material__item" data-range-size="2000">
        <div class="material__media media">
          <i class="media__background media__background--contain"
            style="background-image:url('../media/material__background--4.png')"></i>
          <a href="../media/material__background--1.png" target="_blank" class="material__view">View
            full size</a>
        </div>
        <div class="material__meta">
          <div class="material__row">
            <span class="material__label">Title</span>
            <span class="material__value">logo_black.png</span>
          </div>
          <div class="material__row">
            <span class="material__label">Language</span>
            <span class="material__value">English</span>
          </div>
          <div class="material__row">
            <span class="material__label">Size</span>
            <span class="material__value">300x300 px</span>
          </div>
          <a href="" class="material__download download"></a>
        </div>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php';
