<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__filters appear" data-appear>
    <a class="app__filter btn" data-filter-trigger href="news">
      <span class="btn__text">All news</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=epica-awards">
      <span class="btn__text">Epica awards</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=headline-makers">
      <span class="btn__text">Headline makers</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=design-plus">
      <span class="btn__text">Design plus</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=creative-cities">
      <span class="btn__text">Creative cities</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=lines-of-work">
      <span class="btn__text">Lines of work</span>
    </a>
    <a class="app__filter btn" data-filter-trigger href="news?category=insights">
      <span class="btn__text">Insights</span>
    </a>
  </div>
  <div class="app__mob-filter appear" data-appear>
    <div class="btn btn--dropdown" tabindex="0">
      <span class="btn__text">All news</span>
      <i class="btn__icon btn__icon--right btn__icon--s"><?php include '../assets/img/icon--caret.svg'; ?></i>
      <select class="btn__select" data-select-link>
        <option value="news?category=epica-awards">Epica awards</option>
        <option value="news?category=headline-makers">Headline makers</option>
        <option value="news?category=design-plus">Design plus</option>
        <option value="news?category=creative-cities">Creative cities</option>
        <option value="news?category=lines-of-work">Lines of work</option>
        <option value="news?category=insights">Insights</option>
      </select>
    </div>
  </div>
  <div class="app__heading app__heading--news title appear" data-appear data-sticky>
    <span class="title__no">01</span>
    <h1>All News</h1>
  </div>
  <section class="app__cards" data-clone-append>
    <div class="app__card card card--featured appear" data-appear>
      <div class="card__media card__media--featured media">
        <i class="media__background" style="background-image:url('../media/card--1.png')"></i>
      </div>
      <div class="card__content card__content--featured">
        <div class="card__title card__title--featured">Fact or fiction: NIKE is a ‘good’ brand</div>
        <div class="card__excerpt">The international Epica Awards are back in the
          Netherlands. The goal of the interactive conference, with participants and partners
          such as Philips, PwC and Euronews, is to answer the question: Will responsibility
          (eventually) kill creativity?</div>
        <div class="card__meta card__meta--featured">
          <div class="card__date card__date--featured">September 13, 2018</div>
          <a href="news" class="card__tag card__tag--featured">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--2.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--3.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--4.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--5.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--6.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--7.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--8.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA award world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--9.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear" data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" style="background-image:url('../media/card--10.png')"></i>
      </div>
      <div class="card__content">
        <div class="card__title">EPICA awards world cup category kicks off again</div>
        <div class="card__meta">
          <div class="card__date">September 13, 2018</div>
          <a href="news" class="card__tag">EPICA NEWS</a>
        </div>
      </div>
      <a href="article" class="card__link"></a>
    </div>
    <div class="app__card card appear is-clone" data-clone-card data-equalize="card" data-appear>
      <div class="card__media media">
        <i class="media__background" data-clone-background></i>
      </div>
      <div class="card__content">
        <div class="card__title" data-clone-title>EPICA awards world cup category kicks off again
        </div>
        <div class="card__meta">
          <div class="card__date" data-clone-date>September 13, 2018</div>
          <a href="news" class="card__tag" data-clone-tag>EPICA NEWS</a>
        </div>
      </div>
      <a href="" class="card__link" data-clone-link></a>
    </div>
  </section>
  <!-- <div class="app__more btn" data-more-trigger="../endpoints/news.php?"> -->
  <div class="app__more btn" data-more-trigger="https://epica.maydream-qa.net/endpoints/articles?page">
    <div class="btn__text">More</div>
    <i class="btn__icon btn__icon--right btn__icon--s">
      <?php include '../assets/img/icon--caret.svg'; ?></i>
  </div>
  <?php include '../partials/foot.php';
