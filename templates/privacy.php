<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__heading appear" data-sticky data-appear>
    <h1>Privacy policy</h1>
  </div>
  <div class="app__expands" data-expand>
    <section class="app__expand expand" data-appear data-expand-item="slug01">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">01</span>
          <h3>Introduction</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug02">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">02</span>
          <h3>How we use your personal data</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug03">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">03</span>
          <h3>Providing your personal data to others</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug04">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">04</span>
          <h3>International transfers of your personal data</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug05">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">05</span>
          <h3>Retaining and deleting personal data</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug06">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">06</span>
          <h3>Amendments</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug07">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">07</span>
          <h3>Your rights</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug08">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">08</span>
          <h3>About cookies</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug09">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">09</span>
          <h3>Cookies that we use</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug10">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">10</span>
          <h3>Cookies used by our service providers</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug11">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">11</span>
          <h3>Our details</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="slug12">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">12</span>
          <h3>Data protection officer</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--text _wysiwyg" data-expand-content>
        <div class="expand__list">
          <div class="expand__item">1.1 We are committed to safeguarding the privacy of our partners
            and clients.</div>
          <div class="expand__item">1.2 This policy applies with respect to any usage of the
            personal data of our website visitors and service users.</div>
          <div class="expand__item">1.3 By using our website and agreeing to this policy, you
            consent to our use of cookies in accordance with the terms of this policy.</div>
          <div class="expand__item">1.4 In this policy, "we", "us" and "our" refer to The Epica
            Awards and its parent company Maydream.</div>
        </div>
      </div>
    </section>
  </div>
  <?php include '../partials/foot.php';
