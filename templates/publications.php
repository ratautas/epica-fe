<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__heading appear" data-sticky data-appear>
    <h1>Publications</h1>
  </div>
  <div class="app__expands" data-expand>
    <section class="app__expand expand" data-appear data-expand-item="book">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">01</span>
          <h3>Epica book</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--book" data-expand-content>
        <div class="expand__book book">
          <div class="book__featured media">
            <img src="../media/book__featured.png" alt="" class="media__image media__image--book">
          </div>
          <div class="book__sections">
            <div class="book__section">
              <div class="book__subtitle">A year of creativity</div>
              <div class="book__text _wysiwyg">
                <p>Have you got the “write” stuff? The Epica Book is a way of finding out. Our jury
                  of writers on marketing, communications and creativity chose all the work featured
                  in its pages.
                  Published by Bloomsbury – the London publisher that discovered Harry Potter – this
                  slice of creative magic has become almost as hotly anticipated as the winners’
                  list itself. That’s because the book is more than a compendium of winning work and
                  high-scoring entries. It also offers exclusive interviews and insights into the
                  evolution of our industry.
                  In this year’s issue, highlights include:
                  <ul>
                    <li>How McCann Worldgroup became a creative powerhouse</li>
                    <li>The living logo that evolves with every story of injustice</li>
                    <li>How a small Polish agency made the world cry</li>
                    <li>The Russian rock star with a talent for eye-popping videos</li>
                    <li>How an influencer scam turned the tables on its audience</li>
                  </ul>
                  The foreword is by Kate Stanners, Chairwoman and Global Chief Creative Officer of
                  Saatchi & Saatchi.
                  The cover is an award-winning print ad from the “Trash Masks” series by Advantage
                  Y&R in Namibia.</p>
              </div>
            </div>
            <div class="book__section">
              <div class="book__media">
                <img src="../media/book__image.png" alt="" class="media__image">
              </div>
              <div class="book__subtitle">Sharing the magic</div>
              <div class="book__text _wysiwyg">
                <p>We are in the middle of a rapid transformation in our business. Nothing will ever
                  be the same, and nor should it. Technology is making advertising more interesting,
                  more potent and more relevant.
                  But we should never lose sight of what remains at our core: fresh ideas. And we
                  must never lose the craft and skills that help us turn those ideas into
                  compelling, entertaining and seductive stories.
                  It is the writer’s words, the director’s vision, the photographer’s eye, the
                  editor’s clarity, the musician’s brevity that turn a simple script into something
                  memorable. It is a designer’s skill that turns a functional customer interface
                  into a sensory experience, to be enjoyed and remembered time after time. This is
                  our magic.
                  We’re in an exciting time, where the media landscape, like everything else, is
                  changing dramatically. The media plan, as it was, is being torn up. Often the most
                  powerful ideas exist between the lines of a plan, where they create their own
                  story. We are increasingly obsessed by earned media, and we earn it by having a
                  brilliant idea that is interesting to people. Then the way that story is shared
                  becomes important. So often now the work we admire isn’t simply in the media, but
                  is reported on and shared by the media.
                  Without press coverage of creative industries, we wouldn’t debate, we wouldn’t
                  challenge either each other, we wouldn’t learn, and we wouldn’t grow. We’d miss
                  out on celebrating our industry’s moments of creative triumph together,
                  collectively. It is journalism that ensures that the stories behind groundbreaking
                  creative ideas are shared publicly in order to challenge and inspire audiences,
                  expanding minds in the process. And journalists are the ones who provide the much
                  needed context, depth and social value to the advertising work that’s created. The
                  journalists are the ones who maintain high standards for us all, holding us
                  accountable for our own work, and for our actions along the way.
                  If you believe, as I do, in the unreasonable power of creativity, these are
                  exciting times. The rules are changing constantly, which is music to the ears of
                  those who are perpetually curious. But some things are constant, such as the power
                  of an original idea, and the craft and skills that bring it to bear. As the rules
                  go out of the window, we should be optimistic about what’s coming up next and
                  celebrate what is exciting right now. I look forward to being challenged by the
                  brilliant creative ideas featured on the pages of publications such as Epica for
                  years to come.</p>
                <p>&nbsp;</p>
                <p>Kate Stanners, Chairwoman and Global Chief Creative Officer of Saatchi & Saatchi
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="tributes">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">02</span>
          <h3>Epica tributes</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--tributes" data-expand-content>
        <div class="expand__tributes tributes">
          <div class="tributes__intro _wysiwyg">
            <p>We’re delighted to present you with our Epica Tributes. These e-books are our way to
              pay tribute to some of the brands and agencies that have been the most successful at
              Epica. Each of them contains all of the brand's or agency network's best Epica entries
              over the past few years. We hope you enjoy them.</p>
          </div>
          <div class="tributes__sections">
            <div class="tributes__section">
              <div class="tributes__subtitle">Brand tributes</div>
              <div class="tributes__grid">
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--pepsico.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--audi.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--samsung.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--unilever.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--sony.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--samsung.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--pepsico.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--audi.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--samsung.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--unilever.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--sony.png')"></span>
                  </a>
                </div>
                <div class="tributes__item">
                  <a class="tributes__trigger btn media" href="tribute">
                    <span class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--samsung.png')"></span>
                  </a>
                </div>
              </div>
            </div>
            <div class="tributes__section">
              <div class="tributes__subtitle">Agency tributes</div>
              <div class="tributes__grid" data-fit>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--mccann.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--ogilvy.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--mccann.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--ogilvy.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--yr.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--mccann.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--yr.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tributes__item" data-fit-item>
                  <div class="tributes__trigger btn media" data-fit-trigger>
                    <div class="media__background media__background--tributes"
                      style="background-image:url('../media/tributes__logo--ogilvy.png')"></div>
                  </div>
                  <div class="tributes__content" data-fit-content>
                    <div class="tributes__wrapper">
                      <div class="tributes__embed">
                        <img src="../media/issu.png" alt="">
                      </div>
                      <div class="tributes__text _wysiwyg">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget
                          ligula
                          porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu
                          gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi
                          aliquam ut lorem vel convallis. Mauris in nibh sit amet ipsum aliquet
                          vestibulum. Morbi posuere lectus ac erat posuere vestibulum. Donec non
                          dolor
                          mi. Etiam lacinia leo vel arcu gravida venenatis. Sed nec justo varius
                          mauris vehicula facilisis. Morbi aliquam ut lorem vel convallis. Mauris in
                          nibh sit amet ipsum aliquet vestibulum. Morbi posuere lectus ac erat
                          posuere
                          vestibulum.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app__expand expand" data-appear data-expand-item="press">
      <div class="expand__top" data-sticky data-expand-trigger>
        <div class="expand__title title">
          <span class="title__no">03</span>
          <h3>Press coverage</h3>
        </div>
        <i class="expand__icon"></i>
      </div>
      <div class="expand__content expand__content--press" data-expand-content>
        <div class="expand__press press">
          <div class="press__embed">
            <iframe
              src="https://e.issuu.com/embed.html?identifier=cn5s9l484ufy&amp;embedType=script#18971689/43078320"
              style="border:none;width:100%;height:100%;" title="issuu.com" allowfullscreen
              webkitallowfullscreen mozallowfullscreen msallowfullscreen></iframe>
          </div>
          <div class="press__content">
            <div class="press__icon" style="background-image:url('../assets/img/press.png')"></div>
            <div class="press__text _wysiwyg">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget ligula
                porttitor mauris viverra volutpat vitae a dui. Etiam lacinia leo vel arcu gravida
                venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi aliquam ut lorem
                vel convallis. Mauris in nibh sit amet ipsum aliquet vestibulum. Morbi posuere
                lectus ac erat posuere vestibulum. Donec non dolor mi. Etiam lacinia leo vel arcu
                gravida venenatis. Sed nec justo varius mauris vehicula facilisis. Morbi aliquam ut
                lorem vel convallis. Mauris in nibh sit amet ipsum aliquet vestibulum. Morbi posuere
                lectus ac erat posuere vestibulum.</p>
            </div>
            <a class="press__more btn" href="coverage">
              <span class="btn__text">See a list</span>
            </a>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php include '../partials/foot.php';
