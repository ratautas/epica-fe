<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <div class="app__picker picker" data-sticky>
    <div class="picker__select" tabindex="0">
      <div class="picker__trigger">
        <div class="picker__label">2018 results</div>
        <i class="picker__caret"><?php include '../assets/img/icon--caret.svg'; ?></i>
      </div>
      <div class="picker__dropdown dropdown">
        <div class="dropdown__list" data-scroller>
          <a href="results?year=2017" class="dropdown__item">2017 results</a>
          <a href="results?year=2016" class="dropdown__item">2016 results</a>
          <a href="results?year=2015" class="dropdown__item">2015 results</a>
          <a href="results?year=2014" class="dropdown__item">2014 results</a>
          <a href="results?year=2013" class="dropdown__item">2013 results</a>
        </div>
      </div>
      <select class="picker__native" data-select-link>
        <option value="results?year=2017">2017 results</option>
        <option value="results?year=2016">2016 results</option>
        <option value="results?year=2015">2015 results</option>
        <option value="results?year=2014">2014 results</option>
        <option value="results?year=2013">2013 results</option>
      </select>
    </div>
  </div>
  <div class="app__abstract">France topped the country rankings, obtaining 53
    awards including 12 golds.</div>
  <div class="app__share app__share--top share">
    <div class="share__label">Share on:</div>
    <div class="share__icons">
      <i class="share__icon">
        <img src="../assets/img/icon--facebook.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--linkedin.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
      <i class="share__icon">
        <img src="../assets/img/icon--twitter.svg" alt="" class="share__image">
        <a href="" class="share__link"></a>
      </i>
    </div>
  </div>
  <div class="app__results results">
    <div class="results__slider swiper-container appear" data-results-slider data-appear>
      <div class="results__wrapper swiper-wrapper">
        <div class="results__slide swiper-slide">
          <div class="results__details results__details--slide">
            <div class="results__subtitle results__subtitle--slide">Grand prix print</div>
            <div class="results__meta results__meta--slide">
              <div class="results__company">DDB Group Germany (Düsseldorf)</div>
              <div class="results__achievements">"Highlight the Remarkable"</div>
            </div>
          </div>
          <div class="results__featured media">
            <i class="media__background media__background--results media__background--contain"
              style="background-image:url('../media/results__image--1.png')"></i>
          </div>
          <a href="award" class="results__link"></a>
        </div>
        <div class="results__slide swiper-slide">
          <div class="results__details results__details--slide">
            <div class="results__subtitle results__subtitle--slide">2Grand prix print</div>
            <div class="results__meta results__meta--slide">
              <div class="results__company">DDB Group Germany (Düsseldorf)</div>
              <div class="results__achievements">"Highlight the Remarkable"</div>
            </div>
          </div>
          <div class="results__featured media">
            <i class="media__background media__background--results"
              style="background-image:url('../media/results__image--1.png')"></i>
          </div>
          <a href="award" class="results__link"></a>
        </div>
        <div class="results__slide swiper-slide">
          <div class="results__details results__details--slide">
            <div class="results__subtitle results__subtitle--slide">3Grand prix print</div>
            <div class="results__meta results__meta--slide">
              <div class="results__company">DDB Group Germany (Düsseldorf)</div>
              <div class="results__achievements">"Highlight the Remarkable"</div>
            </div>
          </div>
          <div class="results__featured media">
            <i class="media__background media__background--results"
              style="background-image:url('../media/results__image--1.png')"></i>
          </div>
          <a href="award" class="results__link"></a>
        </div>
        <div class="results__slide swiper-slide">
          <div class="results__details results__details--slide">
            <div class="results__subtitle results__subtitle--slide">4Grand prix print</div>
            <div class="results__meta results__meta--slide">
              <div class="results__company">DDB Group Germany (Düsseldorf)</div>
              <div class="results__achievements">"Highlight the Remarkable"</div>
            </div>
          </div>
          <div class="results__featured media">
            <i class="media__background media__background--results"
              style="background-image:url('../media/results__image--1.png')"></i>
          </div>
          <a href="award" class="results__link"></a>
        </div>
        <div class="results__slide swiper-slide">
          <div class="results__details results__details--slide">
            <div class="results__subtitle results__subtitle--slide">5Grand prix print</div>
            <div class="results__meta results__meta--slide">
              <div class="results__company">DDB Group Germany (Düsseldorf)</div>
              <div class="results__achievements">"Highlight the Remarkable"</div>
            </div>
          </div>
          <div class="results__featured media">
            <i class="media__background media__background--results"
              style="background-image:url('../media/results__image--1.png')"></i>
          </div>
          <a href="award" class="results__link"></a>
        </div>
      </div>
      <div class="results__state"></div>
      <i class="results__nav results__nav--prev">
        <span class="results__arrow results__arrow--prev arrow"></span>
      </i>
      <i class="results__nav results__nav--next">
        <span class="results__arrow results__arrow--next arrow"></span>
      </i>
    </div>
    <div class="results__blocks">
      <div class="results__block appear" data-appear>
        <div class="results__media media">
          <i class="media__background media__background--results"
            style="background-image:url('../media/results__image--2.png')"></i>
        </div>
        <div class="results__details">
          <div class="results__subtitle" data-equalize="results-subtitle">Agency of the year</div>
          <div class="results__meta">
            <div class="results__company">Forsman & Bodenfors</div>
            <div class="results__achievements">10 AWARDS including 5 Golds</div>
          </div>
        </div>
        <a href="award" class="results__link"></a>
      </div>
      <div class="results__block appear" data-appear>
        <div class="results__media media">
          <i class="media__background media__background--results"
            style="background-image:url('../media/results__image--2.png')"></i>
        </div>
        <div class="results__details">
          <div class="results__subtitle" data-equalize="results-subtitle">Network of the year</div>
          <div class="results__meta">
            <div class="results__company">McCann worldgroup</div>
            <div class="results__achievements">37 AWARDS including 1 Grand Prix and 9 Golds</div>
          </div>
        </div>
        <a href="award" class="results__link"></a>
      </div>
      <div class="results__block appear" data-appear="1000">
        <div class="results__media media">
          <i class="media__background media__background--results"
            style="background-image:url('../media/results__image--2.png')"></i>
        </div>
        <div class="results__details">
          <div class="results__subtitle" data-equalize="results-subtitle">Production company of the
            year</div>
          <div class="results__meta">
            <div class="results__company">Blur Films</div>
            <div class="results__achievements">Highest scoring film entered by a production company
            </div>
          </div>
        </div>
        <a href="award" class="results__link"></a>
      </div>
    </div>
    <div class="results__listing listing">
      <div class="listing__title">
        <h3>Category</h3>
      </div>
      <div class="listing__sections">
        <section class="listing__section">
          <div class="listing__letter">A</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Advertising
              photography</a>
            <a href="awards" class="listing__item appear" data-appear="40">Alcoholic drinks</a>
            <a href="awards" class="listing__item appear" data-appear="40">Animation</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">B</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Best use of music</a>
            <a href="awards" class="listing__item appear" data-appear="40">Best use of sound</a>
            <a href="awards" class="listing__item appear" data-appear="40">Brand identity</a>
            <a href="awards" class="listing__item appear" data-appear="40">Branded games</a>
            <a href="awards" class="listing__item appear" data-appear="40">Business to business
              direct</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">C</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Copywriting &
              storytelling</a>
            <a href="awards" class="listing__item appear" data-appear="40">Corporate image</a>
            <a href="awards" class="listing__item appear" data-appear="40">Creative technology</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">D</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Direction &
              cinematography</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">E</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Event</a>
            <a href="awards" class="listing__item appear" data-appear="40">Experiential & shopper
              marketing</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">F</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Films & series</a>
            <a href="awards" class="listing__item appear" data-appear="40">Financial services</a>
            <a href="awards" class="listing__item appear" data-appear="40">Food</a>
            <a href="awards" class="listing__item appear" data-appear="40">Films & series</a>
            <a href="awards" class="listing__item appear" data-appear="40">Fashion, footwear &
              personal
              accessories</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">H</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Health & beauty</a>
            <a href="awards" class="listing__item appear" data-appear="40">Homes, furnishings &
              appliances</a>
            <a href="awards" class="listing__item appear" data-appear="40">Household maintenance</a>
            <a href="awards" class="listing__item appear" data-appear="40">Humour in advertising</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">I</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Illustration</a>
            <a href="awards" class="listing__item appear" data-appear="40">Integrated campaigns</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">L</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Luxury & premium
              brands</a>
            <a href="awards" class="listing__item appear" data-appear="40">Luxury & premium
              brands</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">M</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Media</a>
            <a href="awards" class="listing__item appear" data-appear="40">Media innovation -
              alternative
              media</a>
            <a href="awards" class="listing__item appear" data-appear="40">Mobile campaigns</a>
            <a href="awards" class="listing__item appear" data-appear="40">Music videos</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">N</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Native advertising</a>
            <a href="awards" class="listing__item appear" data-appear="40">Non-alcoholic drinks</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">O</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Online & mobile
              services</a>
            <a href="awards" class="listing__item appear" data-appear="40">Online ads</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">P</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Packaging design</a>
            <a href="awards" class="listing__item appear" data-appear="40">Personal electronics &
              devices</a>
            <a href="awards" class="listing__item appear" data-appear="40">Post production & visual
              effects</a>
            <a href="awards" class="listing__item appear" data-appear="40">Prescription and otc
              products</a>
            <a href="awards" class="listing__item appear" data-appear="40">Print craft</a>
            <a href="awards" class="listing__item appear" data-appear="40">Product & brand
              integration</a>
            <a href="awards" class="listing__item appear" data-appear="40">Product design</a>
            <a href="awards" class="listing__item appear" data-appear="40">Professional products &
              services</a>
            <a href="awards" class="listing__item appear" data-appear="40">Promotions &
              incentives</a>
            <a href="awards" class="listing__item appear" data-appear="40">Public interest -
              environment</a>
            <a href="awards" class="listing__item appear" data-appear="40">Public interest - health
              &
              safety</a>
            <a href="awards" class="listing__item appear" data-appear="40">Public interest -
              social</a>
            <a href="awards" class="listing__item appear" data-appear="40">Public relations</a>
            <a href="awards" class="listing__item appear" data-appear="40">Public design</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">R</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Radio advertising</a>
            <a href="awards" class="listing__item appear" data-appear="40">Recreation & leisure</a>
            <a href="awards" class="listing__item appear" data-appear="40">Restaurants, bars &
              cafes</a>
            <a href="awards" class="listing__item appear" data-appear="40">Retail services</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">S</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Social networks</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">T</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Transport & tourism</a>
            <a href="awards" class="listing__item appear" data-appear="40">Topical & real-time
              advertising</a>
          </div>
        </section>
        <section class="listing__section">
          <div class="listing__letter">V</div>
          <div class="listing__items">
            <a href="awards" class="listing__item appear" data-appear="40">Virtual reality</a>
            <a href="awards" class="listing__item appear" data-appear="40">Websites</a>
            <a href="awards" class="listing__item appear" data-appear="40">World cup advetising</a>
          </div>
        </section>
      </div>
    </div>
    <a class="results__more btn" href="awards">
      <span class="btn__text">See all awards</span>
      <i class="btn__icon btn__icon--right btn__icon--s">
        <?php include '../assets/img/icon--caret.svg'; ?>
      </i>
    </a>
  </div>
  <?php include '../partials/foot.php';
