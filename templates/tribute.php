<?php include '../partials/head.php'; ?>
<?php include '../partials/header.php'; ?>
<main class="app__container appear" data-page>
  <a class="app__back back appear" data-appear href="publications#tributes">
    <i class="back__arrow arrow"></i>
    <span class="back__label">Back to tributes</span>
  </a>
  <div class="app__heading appear" data-appear data-sticky>
    <h1>Pepsico</h1>
  </div>
  <div class="app__tribute tribute">
    <div class="tribute__text _wysiwyg">
      <p>We’re delighted to present this Epica Brand Tribute to Pepsico.</p>
      <p>Along with the book we sent to Pepsico’s marketing team, this ebook is
        our way of paying tribute to one of the brands that has been the most
        successful at Epica.</p>
      <p>Prefaced by Mark Tungate, Epica's Editorial Director, the book contains
        all of the brand’s best Epica entries over the past few years.</p>
      <p>We hope you enjoy it.</p>
    </div>
    <div class="tribute__embed">
      <iframe
        src="https://e.issuu.com/embed.html?identifier=jd1y368j9nxl&amp;embedType=script#18971689/52634539"
        style="border:none;width:100%;height:100%;" title="issuu.com" allowfullscreen
        webkitallowfullscreen mozallowfullscreen msallowfullscreen></iframe>
    </div>
  </div>
  <?php include '../partials/foot.php';
